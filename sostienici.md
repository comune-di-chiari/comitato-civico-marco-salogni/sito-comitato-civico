---
layout: container
title: Sostienici
---
## Politic-ally

Oggi più che mai, abbiamo bisogno del vostro supporto per portare avanti un progetto politico che promuove trasparenza, inclusione e sviluppo sostenibile per la nostra Città.

Ecco perché chiediamo il vostro aiuto. Contribuendo alla nostra [campagna di crowdfunding](https://www.politic-ally.it/campaign/660daefb52e9357b9293c2cd), non solo sostenete un movimento dedicato al miglioramento di Chiari, ma diventate parte attiva di questo cambiamento. Ogni contributo, grande o piccolo, fa la differenza e ci avvicina un passo in più alla realizzazione dei nostri obiettivi comuni

**Grazie per il vostro sostegno!**