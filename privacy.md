---
layout: container
title: Privacy
subtitle: Per noi la tua privacy è importante
---
### Informativa sulla Privacy

Data di ultimo aggiornamento: 18/04/2024

Grazie per aver visitato il nostro sito web. Ci impegniamo a proteggere i tuoi dati personali e a garantire la tua privacy. Ti preghiamo di leggere attentamente la nostra informativa sulla privacy per comprendere come raccogliamo, utilizziamo e proteggiamo i tuoi dati personali.

#### Raccolta e Uso dei Dati
Quando compili il modulo di contatto sul nostro sito web, raccogliamo le informazioni personali che fornisci, come il tuo nome, indirizzo email e qualsiasi altra informazione che decidi di includere nel campo del messaggio. Questi dati vengono raccolti esclusivamente allo scopo di rispondere alle tue domande e assisterti secondo le tue necessità.

#### Conservazione dei Dati
I dati personali che fornisci tramite il nostro modulo di contatto sono conservati su una piattaforma conforme al Regolamento Generale sulla Protezione dei Dati (GDPR). Assicuriamo che la piattaforma aderisca ai più alti standard di protezione dei dati e di sicurezza per salvaguardare le tue informazioni da accessi non autorizzati, alterazioni o distruzioni.

#### Riservatezza dei Dati
I tuoi dati personali verranno utilizzati esclusivamente per lo scopo sopra indicato e non verranno condivisi con terze parti. Non vendiamo, distribuiamo né affittiamo le tue informazioni personali ad altri. I tuoi dati sono trattati con la massima riservatezza e utilizzati solo per rispondere alla tua richiesta come inviato tramite il nostro modulo di contatto.

#### I Tuoi Diritti
In base al GDPR, hai diversi diritti relativi ai tuoi dati personali:

- Diritto di Accesso: Puoi richiedere una copia dei dati personali che deteniamo su di te.
- Diritto di Rettifica: Puoi richiedere che correggiamo eventuali dati inaccurati o incompleti che deteniamo su di te.
- Diritto alla Cancellazione: Puoi richiedere che cancelliamo i tuoi dati personali quando non sono più necessari per gli scopi per cui sono stati raccolti.
- Diritto di Limitazione del Trattamento: Puoi richiedere che limitiamo il trattamento dei tuoi dati personali in determinate circostanze.
- Diritto di Opposizione: Hai il diritto di opporsi al trattamento dei tuoi dati personali in determinate circostanze.
- Se desideri esercitare uno qualsiasi di questi diritti, ti preghiamo di contattarci secondo le modalità che puoi leggere di seguito.

#### Modifiche alla Nostra Informativa sulla Privacy
Potremmo aggiornare questa informativa sulla privacy di tanto in tanto. La data di efficacia in alto a questa pagina indica quando questa informativa sulla privacy è stata rivista l'ultima volta. Qualsiasi modifica apportata alla nostra informativa sulla privacy in futuro sarà pubblicata su questa pagina.

#### Contattaci
Se hai domande o preoccupazioni riguardo alle nostre pratiche di privacy o ai tuoi dati personali, ti preghiamo di contattarci a: [{{ site.data.info.email }}](mailto:{{ site.data.info.email }}), tramite il nostro modulo di contatto.