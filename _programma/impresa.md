---
title: Impresa
subtitle: " Innovazione e Sostenibilità per un Futuro Competitivo"
excerpt: Chiari, una città con un'ampia varietà di imprese, mira a incrementare
  la competitività locale attraverso varie iniziative. Si prevede il
  monitoraggio e il miglioramento della viabilità nelle aree agricola e
  industriale, con un occhio alla segnaletica e al verde. Le politiche di
  riduzione di tasse e tariffe saranno ampliate, soprattutto per le aziende più
  virtuose, e verrà potenziato il servizio di raccolta rifiuti speciali. Un
  "Progetto semplificazione imprese" faciliterà le pratiche comunali per imprese
  e artigiani. Inoltre, si promuoverà l'insediamento di nuove aziende innovative
  e sostenibili e si valuterà la creazione di un incubatore tecnologico e spazi
  di coworking per giovani imprenditori, utilizzando strutture comunali
  esistenti. Infine, si collaborerà con associazioni e istituti per promuovere
  l'imprenditoria agricola, enfatizzando l'agricoltura biologica e posizionando
  Chiari come polo di eccellenza agroalimentare.
order: 12
---
## P﻿remessa

Chiari è una città ricca di attività economiche molto diversificate: industrie, attività artigianali, imprese agricole, attività commerciali e di servizio operanti in molteplici settori. Questa pluralità ha aiutato nell’affrontare la crisi dello scorso decennio. Le imprese clarensi chiedono di essere poste, anche a livello locale, nella migliore condizione per poter diventare più competitive sul mercato. 

## P﻿roposte

85. Monitorare la viabilità dell’area agricola e dell’area industriale al fine di valutare gli interventi necessari anche con riferimento al verde e alla segnaletica stradale per renderla maggiormente rispondente alle esigenze delle molte attività economiche insediate sul territorio.
86. Ampliare le politiche di riduzione di tasse e tariffe, soprattutto per le aziende più virtuose.
87. Potenziare il "porta a porta" attivando un servizio di raccolta rifiuti speciali presso le aziende agricole e industriali.
88. Creare un “Progetto semplificazione imprese” che faciliti l’espletamento delle pratiche di competenza comunale relative ad imprese, artigiani, agricoltori, commercianti;
89. Promuovere l’insediamento di nuove aziende, specialmente quelle innovative e a basso impatto ambientale.
90. Censire le realtà agricole del territorio al fine di tradurre in essere azioni tese a valorizzare la filiera corta, sostenendone eventualmente anche la coltivazione diversificata e sostenibile;
91. Valutare un incubatore tecnologico dedicato a start-up e spazi di coworking per l’imprenditoria giovanile, valutando la possibilità di valorizzare strutture comunali poco utilizzate.
92. Collaborare con le associazioni di settore e con l’istituto Einaudi per la promozione dell’imprenditoria nel settore agricolo, con particolare attenzione alle coltivazioni biologiche ed alla promozione di Chiari come polo dell’eccellenza agroalimentare in grado di raccordare la Franciacorta e la Bassa Bresciana.