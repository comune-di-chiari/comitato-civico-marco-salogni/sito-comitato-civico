---
title: Anziani
subtitle: Assistenza e Autonomia
excerpt: Chiari affronta le sfide dell’invecchiamento della popolazione con
  servizi innovativi per gli anziani. Si propone la residenzialità assistita per
  over 65 in fragilità, offrendo supporto socio-sanitario in un ambiente sicuro.
  Alloggi protetti e condivisi mirano a sostenere l'autonomia e ridurre
  l'isolamento. La collaborazione con l’Istituto Pietro Cadeo e volontari locali
  rafforzerà le risposte assistenziali. Si prevedono corsi di formazione per
  assistenti agli anziani, inclusa la lingua italiana, e un portale per
  collegare la domanda e offerta di assistenza. Infine, si combatterà la povertà
  digitale con formazione supportata da facilitatori.
order: 17
---
## P﻿remessa

Gli anziani rappresentano il collante tra passato e presente, tra memoria e innovazione; l'età mediana europea è aumentata dai 38 anni nel 2001 fino ai 44 anni nel 2020. Chiari deve pertanto fare fronte alle esigenze, alle ambizioni e alle numerose sfide connesse con tale dato con risposte rapide e soluzioni innovative.

## P﻿roposte

128. Fornire servizi di Residenzialità assistita, rivolta ad anziani con età uguale o superiore a 65 anni  che si trovino in situazione di fragilità e/o autosufficienza parziale e  che preveda l’erogazione, in un ambiente controllato e protetto, di servizi di natura socio sanitaria ed assistenziale utili a supportare il mantenimento delle capacità residue della persona, con l’obiettivo di ritardare il declino delle condizioni psicofisiche e di socialità;
129. Fornire alloggi protetti, ossia soluzioni abitative di vita autonoma che si rivolgono alle persone fragili (in primis anziani, ma non solo), con l’obiettivo di supportarne le necessità, garantendo al contempo protezione e vita di relazione;
130. Prevedere la creazione di alloggi condivisi (minialloggi) che siano finalizzati a ridurre il disagio e l’isolamento sociale nonché a garantire piena dignità alle persone in difficoltà;
131. Implementare la collaborazione con l’Istituto Pietro Cadeo e le numerose realtà di volontariato e assistenza presenti sul territorio clarense  al fine di supportare, coordinare e progettare le migliori risposte sul tema;
132. Il Comune si impegnerà a promuovere l’organizzazione e/o certificare con idoneo attestato la frequenza di corsi di formazione per assistenza agli anziani, anche in collaborazione con le realtà esistenti, che ne attesti le competenze e, non secondario, la conoscenza della lingua italiana con particolare attenzione ai termini tecnici e specifici correlati all’assistenza agli anziani; 
133. Sponsorizzare e rendere operativo l’incontro fra la domanda di assistenza agli anziani ovvero persone disabili e offerta di lavoro quali colf/badanti attraverso un portale/sportello facilmente consultabile e fruibile sul sito del Comune di Chiari, nel quale dovranno essere indicate le competenze raggiunte nonché la conoscenza della lingua italiana; 
134. Combattere la “povertà digitale” attraverso percorsi di formazione da promuoversi di concerto con il Comune, anche attraverso la presenza di un facilitatore.