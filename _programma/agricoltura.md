---
title: Agricoltura
subtitle: Coltivare Innovazione, Raccogliere Sostenibilità
excerpt: Chiari conta circa 200 aziende agricole essenziali per l'economia
  locale. La nuova Amministrazione punta a rendere l'agricoltura più sostenibile
  e responsabile, non solo mantenendo, ma anche innovando le pratiche agricole.
  Si propone una gestione efficiente dei rifiuti agricoli tramite Chiari
  Servizi, in cambio di una tariffa forfettaria, e una gestione del cippato in
  collaborazione con aziende specializzate. Si intende anche migliorare la
  viabilità agricola, adattandola alle esigenze moderne e aumentando la
  sicurezza. Altre misure includono un "sportello amico" per semplificare la
  burocrazia, il recupero dell'edificato agricolo abbandonato, una gestione
  attenta dello smaltimento dei liquami, e l'uso responsabile di fertilizzanti e
  diserbanti, promuovendo prodotti ecocompatibili.
order: 6
---
## P﻿remessa

A Chiari ci sono circa 200 aziende agricole che giocano ancora un ruolo importante all’interno del tessuto socioeconomico cittadino e possono essere il volano per una gestione più ecosostenibile della Città, con effetti che possono estendersi anche gli altri settori ad essa connessi. L’idea di una campagna che inquina e non rispetta la disciplina in materia deve essere abbandonata verso un approccio più concreto, più vicino alle sue esigenze e che si traduca anche in una forma di maggiore responsabilizzazione degli operatori economici del settore. Pur in un contesto dove l’abbandono dell’attività rurale è sempre più marcato, ciò non implica che ci se ne debba dimenticare, lasciando gli agricoltori e gli allevatori clarensi alle prese con la gestione della terra, degli animali e della giungla amministrativa che, dato il settore di primaria importanza, gode di incentivi spesso non sfruttati appieno. La nuova Amministrazione dovrà innanzitutto farsi portatrice ed interlocutrice delle istanze rurali, ma dovrà anche porsi quale entità innovatrice e promotrice di una gestione efficiente e ambientalmente compatibile dell’azienda agricola, responsabilizzandola senza per questo divenire giudice severo e insensibile alla quotidiana gestione dell’azienda.

## P﻿roposte

43. Favorire una gestione efficiente, a costo zero per l’Amministrazione cittadina e burocraticamente snella per l’azienda agricola, della gestione dei rifiuti. Valorizzando le attività compatibili della Municipalizzata Chiari Servizi - che ha nel suo oggetto sociale la possibilità di gestire tale tipologia di rifiuti – gli agricoltori e gli allevatori potrebbero, in cambio di una somma forfettaria parametrata alla consistenza della propria azienda, liberarsi periodicamente di tali rifiuti che verrebbero quindi gestiti e smaltiti da Chiari Servizi. Ciò, oltre che favorire una gestione oculata dei rifiuti e una visione globale della loro produzione, scoraggia anche comportamenti ambientalmente non sostenibili (oltre che illegali) quali i non rari fuochi di questi imballaggi.
44. Promuovere una gestione efficiente del cippato: con la collaborazione (da richiedere) di aziende specializzate nel settore, l’agricoltore potrebbe avvalersi degli operatori che lavorano i residui lignei delle potature, facendo confluire il cippato presso un’area dedicata  nei pressi della Municipalizzata. L’intervento potrebbe, in seguito, rendersi autonomo da parte del Comune una volta che si fosse dotato dei pochi ma necessari mezzi per provvedervi.
45. Manutenere costantemente e ripensare la viabilità agricola. Le c.d. “stradine di campagna” se un tempo erano vie sufficienti per permettere l’attività agricola, ora, in ragione della meccanizzazione del settore e della loro sempre più alta percorribilità rischiano di essere insicure e non più adatte ai mezzi sempre più imponenti. All’inadeguatezza si aggiunge l’insicurezza data anche dalla stessa conformazione delle strade e dalla presenza sulle sponde di alberi che, soprattutto in certi momenti dell’anno, compromettono la visibilità e rischiamo, oltre che rallentare l’attività agricola, anche l’incolumità degli operatori o dei semplici passanti.La nuova Amministrazione vorrebbe quindi curare, in un’ottica di costante presenza nelle zone agricole, che le manutenzioni pubbliche e private vengano compiute, nel rispetto delle esigenze di crescita colturale e aziendali.
46. Promuovere la presenza sul territorio e rilanciare lo sviluppo agricolo favorendo la pubblicità e la semplificazione burocratica delle pratiche mediante un servizio costante di “sportello amico” a cui le imprese agricole potranno rivolgersi per sbrigare la burocrazia e apprendere nuove opportunità di sviluppo.
47. Considerare il riutilizzo, il recupero incentivato e la rigenerazione dell’edificato agricolo abbandonato, favorendo una gestione attenta e tutelata del consumo di suolo agricolo.
48. Gestire, coerentemente con l’incremento degli allevamenti intensivi, la superficie dedicata allo smaltimento dei liquami, evitando di recarsi in altre provincie.
49. Promuovere un utilizzo, oculato e rispettoso delle norme applicabili, di fertilizzanti, ammendanti e diserbanti, favorendo ed incentivando anche l’utilizzo di prodotti sempre più ecocompatibili ed al tempo stesso efficienti per la crescita colturale, con particolare attenzione all’uso attento dei diserbanti  su cigli e scarpate di strade, fossi e canali.