---
title: Sicurezza e Legalità
subtitle: Sicurezza e serenità per tutti
excerpt: A Chiari, garantire la sicurezza cittadina è prioritario per mantenere
  la città vivibile e attrattiva. Tra le misure proposte, vi è l'aumento
  dell'organico della Polizia Locale per raggiungere la densità di un agente
  ogni 1500 abitanti, come suggerito dalla Regione Lombardia. Si prevede
  l'introduzione di ausiliari del traffico per ottimizzare il controllo dei
  parcheggi, specialmente quelli riservati ai disabili. Altre iniziative
  includono l'aggiornamento dell'illuminazione pubblica, l'installazione di
  ulteriori telecamere di sicurezza e la creazione di una Commissione Speciale
  Sicurezza per monitorare e prevenire la microcriminalità. Si punta anche a
  intensificare la collaborazione con Chiari Servizi per combattere l'abbandono
  di rifiuti e a promuovere l'utilizzo sicuro di spazi pubblici, come la zona
  della stazione ferroviaria. Inoltre, si lavorerà con le scuole per progetti
  educativi su sicurezza stradale e anti-bullismo.
order: 14
---
## P﻿remessa

Tema da sempre sentito importante – correttamente – è quello della sicurezza della nostra Città. Poter garantire una Chiari sicura, di giorno quanto di notte, per i suoi cittadini è un elemento essenziale per poter rendere davvero una città vivibile, accogliente ed attrattiva, sia a livello di sicurezza personale, sia in riferimento alla tutela delle attività economico-sociali.

## P﻿roposte

101. Implementare l’organico del locale Corpo di Polizia Locale per rientrare nella media di un Agente ogni 1500 abitanti suggerito da Regione Lombardia.
102. Introdurre quanto meno due Ausiliari del traffico per disimpegnare la Polizia Locale dall’ordinario controllo dei parcometri, garantendo un maggiore controllo anche rispetto agli stalli di sosta dedicati alle persone disabili per garantirne la fruibilità a coloro che ne hanno diritto.
103. Ripensare e sostituire ove necessario l’illuminazione del centro storico e della periferia coinvolgendo ove possibile anche i quartieri interessati.
104. Installare telecamere a controllo delle zone sensibili della città oltre alle ottanta già installate;
105. Creare una Commissione Speciale Sicurezza composta da 3 membri di cui un Consigliere di Maggioranza, un Consigliere di Opposizione ed il Sindaco, con lo scopo di riunirsi periodicamente anche con le Forze di Pubblica Sicurezza per analizzare eventuali situazioni da attenzionare e per prevenire, lavorando insieme, fenomeni di microcriminalità o disagio.
106. Incrementare la collaborazione tra la Polizia Locale e Chiari Servizi per il contrasto all’abbandono di rifiuti sul Territorio valutando l’inasprimento delle sanzioni amministrative a carico di coloro che abbandonano rifiuti sul Territorio.
107. Rendere maggiormente vissute e controllate alcune zone percepite insicure in Città, come la Stazione Ferroviaria e zone limitrofe, affinché si possano valorizzare gli spazi interni al servizio di alcune realtà associative del territorio.
108. Lavorare con gli Istituti Secondari di Primo e Secondo Grado per creare progetti di sensibilizzazione rispetto alla corretta circolazione stradale ed al contrasto di episodi di bullismo.