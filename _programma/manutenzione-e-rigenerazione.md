---
title: Manutenzione e Rigenerazione
subtitle: Valorizziamo quello che già abbiamo
excerpt: Chiari punta a mantenere la sua vivibilità attraverso un uso attento
  dei 2 milioni di Euro del PNRR per rilanciare le strutture esistenti,
  limitando l'uso del suolo pubblico. Verrà pianificata una manutenzione
  preventiva di strade, spazi verdi e strutture pubbliche per evitare interventi
  d'emergenza. Si effettuerà una mappatura del patrimonio immobiliare per
  programmare manutenzioni e rigenerazioni. Inoltre, si manterranno i corsi
  d'acqua e si valuterà il recupero della cava “Ex Fin-Beton” per un impianto
  fotovoltaico e una Comunità Energetica Rinnovabile, con possibile
  trasformazione di aree residuali in boschi.
order: 8
---
## P﻿remessa

La nostra Città per mantenere un alto grado della sua vivibilità necessita, come per ogni altra Città, di una costante manutenzione e cura. Tali attività sono sempre quindi necessarie e ricoprono una parte importante del bilancio di ogni comune. La poliedricità degli interventi può e deve essere in ogni caso gestita nel modo più efficiente possibile, sia sotto il profilo degli interventi concreti, sia sotto il profilo economico. In tale prospettiva, diventa quindi fondamentale partire dal patrimonio mobiliare e immobiliare esistente, rigenerandolo mediante l’utilizzo dei fondi messi a disposizione. Ci deve essere la consapevolezza che l’esistente deve essere valorizzato mediante interventi di manutenzione intelligenti ed oculati che sappiano quindi fornire soluzioni concrete alla cittadinanza, vera destinataria di tali opere e che è necessario rendere protagonista in quanto prima fruitrice dei benefici che si traggono dal dare nuova vita alle cose.

## P﻿roposte

57. Utilizzo oculato e consapevole dei fondi del PNRR – stimati in circa 2 milioni di Euro nei prossimi 5 anni – al fine di rilanciare le strutture già esistenti, nell’ottica di limitare l’utilizzo di suolo pubblico secondo le direttive comunitarie. L’intento non è quello di limitare le potenzialità di crescita demografica ed economica di Chiari, ma cogliere nuove opportunità derivanti dalle risorse finanziarie a disposizione e da una migliore gestione del patrimonio urbanistico già presente.
58. Effettuare, nel corso dei 5 anni di mandato ed al netto dei vincoli di bilancio, una manutenzione preventiva ragionata e programmata dei fondi stradali, del verde e delle strutture pubbliche, con l’intento di evitare gli interventi di manutenzione urgente ed emergenziale. A tal fine, la nuova Amministrazione terrà conto del livello di degrado e del tempo di obsolescenza dei materiali e delle opere pubbliche da manutenere, col fine di prevedere una calendarizzazione degli interventi.
59. Effettuare una mappatura del patrimonio immobiliare e mobiliare pubblico in disponibilità del Comune, al fine di programmarne la manutenzione e la rigenerazione.
60. Integrare i servizi strumentali di manutenzione degli edifici pubblici comunali riducendo eventuali tempi d’intervento e programmazione, cercando di intervenire maggiormente sulla prevenzione delle attività da eseguire.
61. Garantire una manutenzione effettiva delle rogge e dei corsi d’acqua presenti nel tessuto urbano di Chiari, favorendo anche opere di piantumazione lungo le sponde del reticolo irriguo minore, al fine di prevenire il deposito di detriti, sabbie e la formazione di accumuli degli stessi, con ciò rilanciando la rigenerazione dell’ambiente circostante,  la funzione estetica, artistica e storica delle acque da cui Chiari trae il suo nome.
62. Poter considerare, previa valutazione con A.R.P.A. delle acque sotterranee di prima falda e, in generale, con lo stato complessivo di salubrità ambientale, il recupero e la rivalutazione della cava “Ex Fin-Beton” con l’insediamento di un impianto fotovoltaico e la creazione di una CER (Comunità Energetica Rinnovabile) composta da cittadini e piccole e medie imprese che condividano energia elettrica rinnovabile prodotta da uno o più soggetti associati alla comunità.
63. Valutare la possibilità di trasformare in aree boschive le aree residuali della TAV e della Brebemi, a compensazione degli ettari cementificati e impermeabilizzati, sottratti alla zona agricola