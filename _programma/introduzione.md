---
title: Introduzione
subtitle: Abbiamo, stiamo e batteremo le strade della nostra Città per essere
  presenti sul territorio, attenti alle sue esigenze, lungimiranti nelle sue
  aspettative e coraggiosi nelle sue sfide.
excerpt: La coalizione a sostegno di Marco Salogni per sindaco di Chiari,
  composta dal "Comitato Civico Marco Salogni", "Patto per Chiari Green" e dal
  "Partito Democratico", punta a una politica centrata sull'ascolto dei
  cittadini. Essi sono considerati sia individualmente sia come parte di una
  comunità in dialogo con altre e con l'ambiente. Le linee guida della
  coalizione, intese come documento dinamico e inclusivo, mirano a promuovere lo
  sviluppo socio-economico sostenibile di Chiari, valorizzando partecipazione
  cittadina, sicurezza, cultura, e supporto alle imprese locali. L'approccio
  amministrativo proposto è integrato e interconnesso, con al centro sempre il
  benessere dei cittadini
order: 1
---
## **L﻿a coalizione**

**La Coalizione per il sostegno della candidatura** a Sindaco di Chiari di Marco Salogni – composta dal “Comitato Civico per Marco Salogni Sindaco”, dalla lista civica“Patto per Chiari Green” e dal “Partito Democratico” – ha espresso nelle seguenti linee programmatiche il suo concetto di Politica, fondato sull’ascolto delle persone della nostra Città che costituiscono il punto di partenza ed il punto d’arrivo del suo agire politico, i protagonisti e l’obbiettivo ultimo a cui è orientato il proprio ragionamento, per trovare le soluzioni e le proposte ai loro problemi, alle loro esigenze e ai loro desideri.*

## L﻿e persone al centro

**La centralità data alle persone** non deve essere solo intesa quale mera posizione programmatica, ma deve essere calata nel concreto, consci che il benessere di ognuno è davvero garantito e raggiunto nel momento in cui lo si considera sia nella sua specialità e singolarità, sia quale membro di una comunità che opera in dialogo con altre comunità - vicine e lontane - sia quando vive e si sviluppa nella realtà naturale che lo circonda.

Se questo è il bersaglio, le frecce nel nostro arco sono il nostro impegno, la nostra energia, le nostre competenze, la nostra intraprendenza, a cominciare da tutti coloro che potranno ricoprire ruoli attivi all’intero dell’azione amministrativa. Riteniamo che ci sia ancora tanto da fare e vogliamo porci in prima linea perché crediamo in una visione futura di una Chiari che riparte dalla valorizzazione dell’esistente, dalla vitalità e partecipazione dei suoi abitanti, dalla potenzialità del suo ambiente e del suo sviluppo socio-economico sostenibile, dalla sua dinamica inclusione e apertura verso l’esterno, dalla sua innata attenzione e assistenza alle difficoltà, dalla sua reale sicurezza e vivibilità, dalla sua vivace istruzione, cultura e arte, dall’operosità delle sue imprese e dei suoi lavoratori, dall’essenzialità delle sue realtà associative. Tasselli di un mosaico poliedrico che sono tutti - nessuno escluso - fondamentali per rendere la nostra Città a misura ed ispirazione delle persone che la respirano.

## A﻿scolto e dialogo costante

Le seguenti linee programmatiche saranno quindi il faro che guiderà l’operato della **nuova Amministrazione**. Ma sono anche da intendersi come un documento aperto, capace di aggiornarsi ed arricchirsi perché è vivo come le persone a cui è rivolto ed a cui presta costante e tenace ascolto e vicinanza; è aperto anche perché è inclusivo, abile ad accogliere le istanze di chi, pur non avendolo redatto, offre spunti di riflessione ed arricchimento a favore della nostra Città e della sua Cittadinanza, nel pieno e reciproco rispetto e stima.

Infine, nella definizione delle presenti linee programmatiche, la precedenza di un tema è solo espositiva e non ne impone una preminenza o preferenza rispetto ad altri. Nella visione globale dell’azione amministrativa che vogliamo porre in essere siamo ben consci dell’integrazione e della interdipendenza delle questioni che interessano la Città. Ogni concetto, infatti, proprio perché pone al centro Chiari ed il Cittadino, si ramifica, parte e giunge ad essi, e nel farlo si interseca con i vari progetti che stiamo portando avanti.

> L’approccio è quindi quello dell’integrazione e dell’interconnessione, ponendo come obbiettivo il benessere della Cittadinanza e della nostra Chiari.