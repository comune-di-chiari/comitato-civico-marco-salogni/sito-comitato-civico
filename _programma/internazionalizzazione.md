---
title: Internazionalizzazione
subtitle: Per una Chiari aperta al mondo
excerpt: Chiari mira a posizionarsi in un contesto internazionale, ampliando
  l'apertura culturale e le opportunità per i suoi cittadini. Tra le iniziative
  previste ci sono il rinnovo e il potenziamento dei programmi di mobilità
  internazionale come Youth in Action ed Erasmus+, per aumentare le esperienze
  all'estero. Si promuoverà il Bando PensoGiovane con un orientamento
  internazionale, incentivando la partecipazione a eventi e festival globali. È
  previsto anche il rafforzamento dei gemellaggi, come quello con Algemesi, per
  arricchire lo scambio culturale e sportivo. Inoltre, si facilitano scambi e
  borse di studio per studio e tirocini all'estero, supportando il
  riconoscimento reciproco di titoli e valutazioni, e si incoraggiano esperienze
  che arricchiscono la mondialità e la comprensione culturale dei partecipanti.
order: 20
---
## P﻿remessa

In un mondo dove tutto è alla portata di mano, dove persone e popolazioni sono in costante contatto fra loro e movimento, dove il “villaggio globale” è la realtà di tutti i giorni, è necessario pensare la nostra Città inserita in un contesto non solo locale o nazionale, ma anche internazionale. Anche nel quotidiano è evidente come la nostra città si stia aprendo a nuove culture, nuove lingue, nuove tradizioni e, sebbene sia fondamentale curare e tutelare le nostre origini, è importante consentire alla propria Cittadinanza di aprirsi al mondo, per una crescita ed arricchimento che sia tanto individuale quanto collettiva. È pertanto necessario che la nuova Amministrazione fornisca gli strumenti di carattere logistico, finanziario e organizzativo, per consentire questa apertura, sia in “uscita” verso il mondo, sia in “entrata”, consentendo che il mondo entri in Città.

## P﻿roposte

149. Rinnovare e potenziare i progetti di mobilità internazionale, quali Youth in Action, Erasmus+ o YouMore consentendo l’ampliamento dell’offerta di esperienze all’estero e potenziandone le attività.
150. Favorire lo sviluppo del Bando PensoGiovane anche in una chiave sempre più internazionale promuovendo esperienze ad eventi e festival internazionali come forme di interazione tra giovani
151. Potenziare il rapporto di gemellaggio con la città di Algemesi affinché, oltre alla positiva esperienza legata allo sport, si intersechino e si rafforzino i rapporti e le interconnessioni anche verso altre attività, favorendo uno scambio di esperienze in entrata ed in uscita, arricchente per tutti.
152. Promuovere la mobilità all’estero, mediante la creazione di borse di studio e scambi per lo svolgimento di periodi di studio e tirocini all’estero, sia a livello universitario, sia a livello di scuola superiore, facilitando le pratiche burocratiche ed il mutuo riconoscimento dei titoli, degli esami e delle valutazioni.
153. Favorire esperienze di mondialità che arricchiscono e aprono la mente delle persone che lo compiono e che, una volta tornate, possano condividere con la comunità le novità apprese, le culture conosciute, i diversi modi di pensare, i contatti personali intrattenuti.