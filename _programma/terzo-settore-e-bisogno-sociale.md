---
title: Terzo Settore e Bisogno Sociale
subtitle: Fare rete per una Comunità più forte e inclusiva
excerpt: Chiari vanta una rete solida del Terzo Settore, con volontari che
  lavorano per prevenire l'emarginazione sociale. Le iniziative sono focalizzate
  sulla prevenzione e il sostegno, integrando servizi sociali e volontariato per
  una rapida presa in carico delle fragilità. Si propone di prevenire il disagio
  sociale con azioni proattive in scuole e parchi, riconoscendo precocemente i
  soggetti fragili. È prevista la creazione di alloggi di edilizia sociale per
  garantire dignità e inclusione, e una collaborazione continua tra Servizi
  Sociali e realtà imprenditoriali per favorire l'impiego dei più vulnerabili.
  Si valorizzano inoltre tradizioni locali come le Quadre e il Palio per
  promuovere l'engagement giovanile.
order: 10
---
## P﻿remessa

Nella nostra Comunità tuttavia è presente una forte rete del Terzo Settore sostenuta da numerosi Volontari che contribuiscono, in sinergia con i Servizi Sociali, ad impedire che si formino situazioni di emarginazione ed esclusione dalla Comunità.

Siamo consapevoli della necessità e dell’esigenza di generare meccanismi che possano rendere maggiormente vicini i Servizi Sociali e le Istituzioni ai Cittadini per consentire una veloce presa in carico delle fragilità.

Le direttrici sono definite da azioni di prevenzione, di sostegno, di collaborazione, di formazione.

## P﻿roposte

73. Prevenire il disagio e promuovere il benessere attraverso iniziative pro attive nei luoghi di vita: scuola, parchi, strada anche attraverso il riconoscimento precoce dei soggetti fragili per evitare o ridurre le possibili conseguenze correlate a questa condizione;
73. Agire di concerto con le numerose associazioni di volontariato clarensi che con grande impegno e dedizione sostengono in vario modo persone in difficoltà, anche attraverso percorsi di collaborazione e messa in rete col servizio sociale di modo che ogni operatore sia in grado di intercettare i bisogni specifici delle persone in difficoltà;
74. Creare alloggi di edilizia sociale, anche in condivisione, per le persone economicamente fragili, che garantiscano dignità alle persone e l’inclusione;
75. Agire di concerto tramite un dialogo costante e se del caso riunioni periodiche con i Servizi Sociali di modo da accompagnare e sostenere i soggetti fragili presenti sul territorio clarense;
76. Collaborare con le realtà imprenditoriali sul territorio in modo da permettere l’eventuale incontro tra domanda e offerta di lavoro che coinvolga anche e in particolar modo i soggetti più fragili;
77. Valorizzare e promuovere quel patrimonio clarense che sono le Quadre e il Palio, favorendo nuove forme di coinvolgimento dei giovani atleti ed atlete, dando una collocazione definitiva agli stands enogastronomici, ovviando alle questioni logistiche di magazzino per lo stoccaggio dei materiali, facilitando la gestione delle pratiche burocratiche ed il reperimento dei fornitori , nonché promuovendo sinergie con le attività locali per esaltare le eccellenze clarensi.