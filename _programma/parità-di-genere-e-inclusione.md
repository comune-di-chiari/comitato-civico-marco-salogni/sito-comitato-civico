---
title: Parità di Genere e Inclusione
subtitle: Rispetto per una Città migliore
excerpt: Chiari si impegna per la parità di genere e l'inclusione, adottando un
  linguaggio inclusivo negli ambiti istituzionali e supportando
  l'imprenditorialità femminile. Si promuoveranno servizi di supporto alla
  genitorialità, come asili nido, e percorsi educativi nelle scuole per
  condividere i ruoli familiari e promuovere l'integrazione delle famiglie
  straniere. Inoltre, si prevede di attivare un dialogo con le scuole e i centri
  antiviolenza, offrire corsi di formazione sulle emozioni e la sessualità, e
  istituire uno Sportello Donna per assistere donne in situazioni di fragilità o
  violenza. Queste iniziative mirano a creare una comunità più equa e integrata.
order: 19
---
## P﻿remessa

La lotta per la parità di genere e la promozione e la tutela dei diritti delle donne è una responsabilità collettiva che richiede progressi rapidi e sforzi da parte delle istituzioni nel suo complesso e pertanto anche della città di Chiari.

## P﻿roposte

139. Adottare un protocollo di linguaggio inclusivo, anche e soprattutto negli ambiti istituzionali che permetta di valorizzare il ruolo delle donne all’interno della vita pubblica.
140. Incentivare e sostenere l’apertura di attività imprenditoriali femminili e sostenibili sul territorio (attraverso fondi stanziati ad hoc ad esempio “donne, innovazione e impresa”).
141. Implementare i servizi socio assistenziali quali asili nido e servizi di aiuto alla genitorialità.
142. Diffondere la cultura della condivisione dei ruoli attraverso, ad esempio, l’incentivo a fruire di congedi e permessi di paternità.
143. Prevedere percorsi educativi, anche attraverso la formazione delle/degli insegnanti e delle educatrici/educatori, e il coinvolgimento della genitorialità a partire dal Nido e dalla Scuola dell’Infanzia.
144. Promuovere la formazione, la condivisione e il dialogo in merito al tema delle emozioni, della corporeità, dell’affettività e della sessualità nelle scuole attraverso la presenza o con un canale diretto con professionisti/e del settore.
145. Favorire corsi di italiano e di avvicinamento culturale per famiglie straniere che spesso, in ragione della mancanza di inserimento e vicinanza di tutta la comunità, rischiano di compromettere l’integrazione dei propri figli all'interno del tessuto sociale cittadino.
146. Considerare l’apertura di uno sportello immigrati che possa permettere agli stessi di essere accompagnati e tutelati nei loro primi passi nella realtà clarense, promovendo strumenti volti al dialogo tra Comune, imprese e associazioni del territorio.
147. Attivare un dialogo tra le scuole medie inferiori e superiori e i centri antiviolenza e antidiscriminazione prevedendo cicli di incontri e di formazione del personale docente per essere in grado di identificare situazioni di molestie, ogni tipo di discriminazione e  fragilità, anche con l’individuazione di un referente unico.
148. Costituzione di uno Sportello Donna, che collabori con enti, servizi socio sanitari, realtà associative e professionisti/e del territorio e che fornisca informazioni quali, ad esempio, informazioni su asili nido e/o strutture per l'infanzia e di prima consulenza alle donne che vivono situazioni di fragilità, violenza, bullismo.