---
title: Cultura
subtitle: Mente e Spazi Aperti, Idee Libere
excerpt: "Chiari, città ricca di storia e cultura, ha molteplici spazi per
  eventi culturali come Villa Mazzotti e il Museo della Città. L'Amministrazione
  punta a valorizzare questi luoghi con una gestione più autonoma e integrata,
  aumentando la loro autonomia e coordinamento. Tra le proposte: rilanciare
  Villa Mazzotti come centro culturale indipendente, espandere l'evento della
  Micro Editoria, riaprire un bar in Villa Mazzotti per accrescere l'attrattiva
  del parco, e gestire efficacemente il nuovo Teatro Sant’Orsola per assicurare
  un'offerta culturale vivace e inclusiva. Si prevede anche l'ampliamento della
  viabilità vicino al teatro e l'utilizzo di spazi aperti per teatri all'aperto,
  rendendo la cultura più accessibile e integrata nella vita cittadina."
order: 16
---
## P﻿remessa

Una Città che non cura e sviluppa le proprie attività culturali e valorizza il proprio patrimonio storico-artistico dimentica un aspetto fondamentale del benessere e dello sviluppo della propria comunità e dei propri abitanti. Chiari ha la fortuna di essere una città ricca di storia, arte e cultura, così come è ricca di associazioni e gruppi di persone che curano, stimolano ed incentivano attività culturali a beneficio della cittadinanza e di ogni altro visitatore. Nè mancano le strutture e gli spazi dove tali attività ed eventi possono concretizzarsi, a cominciare dalla Villa Mazzotti, dalla Fondanzione Morcelli-Repossi, al Museo della Città, alle varie associazioni fino ad arrivare al nuovo teatro Sant’Orsola di prossima apertura. La nuova Amministrazione è ben conscia di questo patrimonio di luoghi, beni e persone, così come è consapevole tuttavia che senza una incentivazione, una promozione ed un coordinamento questo potenziale si sostanzierebbe solo in un punto programmatico ed il rilancio della Cultura nella nostra bella Città rimarrebbe solo lettera morta.

## P﻿roposte

121. Ripensare il ruolo della Villa Mazzotti nel contesto dello sviluppo culturale cittadino, per una sua maggiore autonomia e per un programma culturale più ricco ed integrato, capace altresì di attrarre le sollecitazioni che arrivano dal territorio (si pensi alle possibili collaborazioni con l’evento delle Mille Miglia, o per la promozione delle Cantine Franciacorta). La nuova Amministrazione si propone di tendere verso una gestione separata rispetto al Comune, cosicché si possa, sempre in coordinamento con il Comune stesso, garantire l’apertura della Villa e lo svolgimento di iniziative per il pubblico, avere poteri di spesa e gestione autonoma e diventare così un contenitore di pregio per la promozione culturale, l’accoglienza di eventi ed  esposizioni in un proficuo collegamento col territorio.
122. Dare slancio all’evento nazionale della Micro Editoria, avviando una interlocuzione con Soprintendenza per valutare la possibilità di utilizzo del piano interrato della Villa padronale sfruttando nel caso le leve del credito sportivo-culturale, per poterlo adibire all’esposizione di ulteriori case editrici o per l’organizzazione e l’ampiamento degli eventi e dei dibattiti che si svolgono durante la chermesse.
123. Considerare la riapertura di un bar all’interno della Villa Mazzotti, per renderla davvero ricettiva e poter godere appieno del bellissimo Parco nel quale è intenzione della nuova Amministrazione curare e ripristinare il percorso vita ormai abbandonato ed in disuso.
124. Garantire una efficiente, vivace, inclusiva e pluralista gestione del nuovo Teatro Sant’Orsola. La disponibilità di questo spazio che la nuova Amministrazione si troverà a gestire ex novo, se da un lato potrebbe apparire una grande potenzialità, dall’altro potrebbe rivelarsi una cattedrale nel deserto se non correttamente amministrata ed organizzata. La nuova Amministrazione, pertanto, si propone di coordinare la gestione del Teatro affinchè le associazioni ed i gruppi che lo vivranno siano parimenti considerate e trovino il loro giusto spazio, favorendo le iniziative espressione del territorio, ma anche, laddove possibile, promuovendo l’afflusso di proposte ed eventi dal capoluogo provinciale o dai Comuni limitrofi, per garantire così anche una programmazione organica, correttamente distribuita nel corso dell’anno, e con eventuali possibilità di autofinanziamento e quindi di risparmio per il Comune di Chiari.
125. Valutare l’ampliamento della viabilità prospiciente il Teatro, per facilitarne l’accesso e l’uscita.
126. Aprire il teatro Sant’Orsola anche alle scuole ed alle associazioni educative, affinché utilizzino gli spazi per attività ed eventi di carattere educativo e di cultura in generale;
127. Considerare la possibilità di creare un “teatro all’aperto”, un luogo di incontro non confinato negli spazi tradizionalmente dedicati, ma accessibile alla Cittadinanza, così che la Cittadinanza stessa possa sentirsi più partecipe delle attività culturali che si propongono e dia anche slancio e vitalità agli spazi pubblici della nostra Città.