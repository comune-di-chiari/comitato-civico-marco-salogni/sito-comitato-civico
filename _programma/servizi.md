---
title: Servizi
subtitle: Efficienza, Inclusione e una Collaborazione tra Comuni
excerpt: "La nuova Amministrazione di Chiari punta a garantire servizi
  efficienti e inclusivi, rimuovendo ostacoli per un'uguaglianza sostanziale tra
  i cittadini. Tra le proposte: affidare a Chiari Servizi la gestione
  cimiteriale e valutare l'estensione dei servizi mensa. Si prevede anche una
  collaborazione con i comuni limitrofi per condividere servizi come l'igiene
  urbana. Un bilancio di sostenibilità aiuterà a migliorare l'impatto delle
  attività. Si propone la creazione di un Banco del Riuso per ridurre i rifiuti
  e un potenziamento del servizio di raccolta porta a porta. Infine, si vuole
  valorizzare la Farmacia Comunale come centro di servizi sociosanitari
  avanzati."
order: 9
---
## P﻿remessa

Il Comune, quale ente locale più prossimo al Cittadino, deve porsi in prima fila nel realizzare e garantire una alta qualità e benessere della propria Cittadinanza, tenendo in considerazione le eterogeneità dei suoi bisogni che variano a seconda dello status sociale, economico, linguistico, fisico. La nuova Amministrazione è ben conscia che offrire soluzioni uguali a situazioni diverse non è il principio d’uguaglianza sancito dalla nostra carta costituzionale e, proprio come richiede la Costituzione, è decisa ad attivarsi per rimuovere gli ostacoli di vario ordine per garantire una uguaglianza davvero sostanziale, a partire dalle fasce di popolazione più bisognose. 

Per raggiungere questi obbiettivi è essenziale quindi concentrarsi sulla disponibilità, efficienza, capillarità dei Servizi che verranno erogati dal Comune, anche in un’ottica di compartecipazione coi Comuni limitrofi e con le realtà associative presenti sul territorio.

## P﻿roposte

64. Affidare a Chiari Servizi, nel corso del prossimo futuro, la gestione dei servizi cimiteriali: tale attività andrebbe a valorizzare ulteriormente i servizi offerti, garantendo una gestione sempre più attenta ad un luogo caro per Cittadinanza;
65. Consentire alla Municipalizzata di integrare i servizi strumentali già effettuati per il Comune di Comezzano Cizzago valutandone l’estensione anche per il Comune di Chiari, in particolare, per il servizio mensa nell’ottica di un aumento della qualità del servizio; 
66. Indirizzare la Municipalizzata verso la creazione di relazioni con i Comuni limitrofi (sull’esempio dei rapporti creati con Comezzano Cizzago) per valutare la condivisione di alcuni servizi, partendo dal servizio d’igiene urbana. Vi è la necessità di creare le condizioni per la costruzione di una rete tra i Comuni dell’Ovest Bresciano con cui si potrebbero condividere servizi che hanno raggiunto una forte efficienza per la nostra Città, per efficientare ulteriormente gli stessi, contenere i costi ed incidere in modo positivo sulle tariffe; 
67. Considerare in un’ottica condivisa coi Comuni limitrofi, le varie proposte di Europrogettazione, strumenti fondamentali per garantire, oltre a tante altre proposte, servizi sempre più efficienti, consentendo che Chiari si possa far portavoce di realtà comunali più piccole che altrimenti non avrebbero le risorse sufficienti per cogliere appieno queste opportunità.
68. Redigere un bilancio di sostenibilità che possa individuare le linee d’azione per migliorare l’impatto economico, ambientale e sociale delle attività della Municipalizzata attraverso un percorso di restituzione e condivisione con la Cittadinanza;
69. Creazione di un Banco del Riuso attraverso la creazione della struttura, sfruttando il bando PNRR ottenuto dalla Municipalizzata. Il servizio vedrà il coinvolgimento delle Associazioni del Territorio per creare un meccanismo di scambio degli oggetti basato sul concetto della banca del tempo. Tale servizio permetterà di intercettare oggetti che altrimenti sarebbero destinati a diventare rifiuto e si inserisce in un percorso di chiusura del ciclo dei rifiuti, già cominciato con la dispensa solidale. In ultimo il servizio permetterà di diminuire i rifiuti ingombranti/indifferenziati che hanno determinato un forte incremento dei costi di smaltimento negli ultimi anni; 
70. Migliorare il servizio di raccolta porta a porta dei rifiuti valutando eventuali interventi, attraverso il supporto tecnico della Società, per la Campagna ed il Centro Storico;
71. Potenziare e migliorare il coordinamento della Municipalizzata con la Polizia Municipale nella lotta e nell’abbandono illecito dei rifiuti;
72. Valorizzare e potenziare il ruolo della Farmacia Comunale affinché possa diventare nel tempo una “Farmacia dei Servizi” seguendo le linee guida di Regione Lombardia, in costante dialogo, nelle rispettive autonomie, con l’ASST per ragionare rispetto ad alcune figure quali l’infermiere di Famiglia e Comunità e per creare  anche uno “Sportello Amico” a fronte di servizi sociosanitari sempre più complessi e di difficile fruizione.