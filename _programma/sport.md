---
title: Sport
subtitle: Coordinamento, inclusione e valorizzazione delle eccellenze
excerpt: Chiari riconosce il ruolo essenziale dello sport per il benessere
  fisico, mentale e sociale dei cittadini. Per migliorare l'accesso e la
  fruizione delle infrastrutture sportive, l'amministrazione propone
  l'istituzione di un coordinamento comunale con una figura manageriale che
  faciliti la collaborazione tra le diverse associazioni sportive. Questo
  coordinamento mira a migliorare l'uso delle strutture come il Campo Sportivo e
  altre infrastrutture, assicurando spazi adeguati e mantenendo le strutture in
  buone condizioni. Si vuole anche promuovere lo sport tra i giovani e gli
  anziani, sostenere l'élite sportiva per attrarre eventi nazionali e
  internazionali, e utilizzare lo sport come strumento di inclusione per persone
  con disabilità. Inoltre, si prevede di sviluppare un sistema viabilistico
  efficiente per gli eventi sportivi e di aumentare l'autonomia e la competenza
  gestionale delle associazioni sportive locali.
order: 13
---
## P﻿remessa

Lo sport quale attività benefica per il corpo, per la mente e per la socialità. Esso è un elemento fondamentale del benessere di ogni Cittadino (di qualsiasi età, sesso, estrazione sociale, capacità fisica e disabilità), così come per la coesione e la solidarietà della Cittadinanza. La nuova Amministrazione è ben consapevole del ruolo giocato dallo sport nella nostra Città, ed altrettanto è conscia delle questioni e delle problematiche che attanagliano la libera espressione e godimento delle infrastrutture sportive presenti sul territorio. Manca infatti un effettivo coordinamento fra le varie associazioni sportive presenti a Chiari, coordinamento necessario per la piena fruizione delle strutture che per troppo tempo sono dipese dalla buona volontà dei singoli, e che non può fisiologicamente offrire un servizio costante, efficiente ed a disposizione di tutti. Inoltre, la piena estrinsecazione dell’attività sportiva deve tenere conto non solo delle diverse tipologie di attività – e quindi delle diverse necessità in termini di servizi e strutture – ma anche della diversità dei singoli cittadini.

## P﻿roposte:

93. Creare un effettivo coordinamento comunale, mediante l’individuazione di una figura facilitatrice di impronta manageriale e di provenienza comunale, distaccato ed integrato ed in costante dialogo con le associazioni sportive, per l’utilizzo delle strutture sportive e della connessa logistica-viabilità. Una maggiore collaborazione, una più efficiente e consapevole volontà di fare rete – anche attraverso la fissazione di riunioni periodiche fra le associazioni - condivisione di idee, spazi e risorse consentirebbe anche un miglioramento collettivo delle associazioni che frequentano il Campo sportivo e di conseguenza del livello delle prestazioni delle singole squadre e atleti. Luogo principale è sicuramente quello del Campo Sportivo, ma la nuova Amministrazione si propone di adottare lo stesso metodo anche in riferimento ad altre strutture sportive presenti in Città, quali quelle site presso la Scuola Toscanini e Polo delle Primarie. In queste strutture sarà anche importante garantire la loro piena fruizione, assicurando per ciascuna associazione idonei spazi di magazzino, docce e spogliatoi funzionanti.
94. Promuovere ed efficientare il coordinamento tra scuola e associazioni che valorizzi la funzione salutare dello sport, con l’obbiettivo di aumentare l’offerta e far conoscere diverse tipologie di attività sportive ai ragazzi, nella consapevolezza del ruolo pedagogico che lo sport può ricoprire, fornendo un modello di comportamento e disciplina utile per la crescita e, in prospettiva, anche mondo del lavoro.
95. Promuovere e sostenere l’attività fisica anche verso le persone più anziane, che vedono nello sport una attività di socialità, riabilitazione e mantenimento del proprio benessere
96. Sostenere lo sport d’elité favorendo, da un lato, i nostri migliori atleti, con gli evidenti vantaggi sia in termini di promozione dei valori intrinseci nello sport, sia quale modello e stimolo per i più giovani; dall’altro, rendere la Città attrattiva verso eventi e competizioni nazionali ed internazionali con evidente vantaggio in termini di visibilità ed attrattività.
97. Valorizzare lo sport quale effettivo strumento di inclusione delle diverse disabilità, favorendo la predisposizione di corsi ad hoc per gli insegnanti che così possono effettivamente dotarsi delle giuste competenze per integrare la diversità nei vari gruppi, evitando la spiacevole e quasi costante marginalizzazione di questa categoria.
98. Curare e manutenere costantemente le strutture sportive, affinché siano accoglienti, ricettive ed idonee alla pratica sportiva. Anche gli impianti più recenti, infatti, necessitano di una manutenzione costante e presentano attualmente una situazione disagiata se non di emergenza per gli atleti.
99. Fornire un sistema viabilistico efficiente soprattutto in occasione degli eventi sportivi, andando a sfruttare i parcheggi integrati di via del consorzio agrario nonché realizzazione di parcheggio a raso in Via del Consorzio Agrario, acquisito da Rete Ferroviaria Italiana.  Sarà altresì necessario, anche avvalendosi dell’attività di gestione e organizzazione della figura del facilitatore in affiancamento alla Polizia Municipale, garantire ovviamente un servizio d’ordine che, una volta chiuso un parcheggio perché pieno, indirizzi ad altro (anche con comunicazione preventiva sui canali sociali del Comune o altro mezzo di comunicazione preventiva).
100. Consentire alle singole associazioni di potersi dotare di maggiore competenza ed autonomia, anche finanziaria: il Comune potrebbe quindi porsi quale collettore per favorire strutture associative più manageriali per meglio gestire, inter alia, anche le iscrizioni, con indubbi vantaggi a favore anche delle associazioni più piccole che con una maggiore capacità di gestione fiscale e tributaria potrebbero realizzare quella riforma dello sport che fino ad oggi manca.