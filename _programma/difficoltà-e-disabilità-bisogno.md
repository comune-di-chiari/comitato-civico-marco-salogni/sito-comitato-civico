---
title: Difficoltà e Disabilità Bisogno
subtitle: Per una Chiari in grado di abbattere Barriere
excerpt: L'amministrazione comunale di Chiari si impegna a rendere la città
  completamente accessibile alle persone con disabilità, eliminando le barriere
  architettoniche e implementando tecnologie abilitanti come l'AI e semafori
  sonori. Si prevede di realizzare percorsi accessibili distintivi,
  digitalizzare i servizi esistenti e attuare il Piano di eliminazione delle
  barriere architettoniche (Peba). Inoltre, si mira a promuovere l'autonomia e
  la socializzazione delle persone con disabilità attraverso la collaborazione
  con associazioni locali, garantendo l'accesso a attività ricreative, sportive
  e culturali per tutti i cittadini, inclusi i bambini con disabilità,
  specialmente durante l'estate.
order: 18
---
## P﻿remessa

L’amministrazione comunale dovrà rendere pienamente fruibili gli spazi pubblici, i luoghi culturali e più in generale, la Città, alle persone con disabilità motoria e cognitiva grazie all’eliminazione delle barriere architettoniche, all’utilizzo dell’AI e di altre tecnologie abilitanti.

## P﻿roposte

135. Garantire l’accesso alle persone con disabilità di tutti i luoghi, attraverso percorsi dedicati e riconoscibili (per esempio anche attraverso i loges per i non vedenti) e arricchendo il patrimonio già esistente attraverso la digitalizzazione (p.e. semafori sonori);
136. Realizzare progetti finalizzati ad abbattere le barriere architettoniche applicando il Peba, Piano di eliminazione delle barriere architettoniche;
137. Favorire l’autonomia e la socialità delle persone con disabilità attraverso il dialogo e la collaborazione con le realtà associative e non presenti sul territorio clarense come altro aspetto della prevenzione del disagio e dell’isolamento sociale;
138. Collaborare con le realtà esistenti sul territorio per garantire anche ai bambini con disabilità pieno accesso alle attività ricreative, sportive, e culturali situate in Città come il Grest, i corsi di sport, di arte e di musica con particolare attenzione al periodo estivo.