---
title: Partecipazione
subtitle: Per una Chiari che Partecipa e Propone
excerpt: La nuova Amministrazione di Chiari si impegna a rendere i cittadini
  protagonisti attivi della loro comunità, per contrastare l'astensionismo e
  rafforzare il coinvolgimento democratico. Propone la creazione di almeno
  quattro Consigli di Quartiere per facilitare la comunicazione e la risposta
  alle esigenze locali, oltre a una ConsultaGiovani per integrare i giovani in
  attività lavorative e creative. Il progetto "Rigenera il tuo Quartiere" mira a
  migliorare infrastrutture e spazi verdi, promuovendo anche il coinvolgimento
  dei cittadini in progetti di rigenerazione urbana e nella revisione del PGT.
  Inoltre, si vuole potenziare il ruolo dei referendum e dei Consiglieri
  Comunali per una partecipazione più diretta e rappresentativa alla vita
  civica.
order: 7
---
## P﻿remessa

Rendere gli abitanti dei quartieri protagonisti dei luoghi che vivono, così da rendere i quartieri veri luoghi di espressione delle persone e della Comunità, e non un mero luogo di sosta fra il lavoro o altre attività personali. Questo consentirà sia di intercettare le esigenze di rinnovamento dei quartieri stessi, ma avrà anche il benefico effetto di far sentire la popolazione ascoltata e coinvolta nelle scelte che più da vicino la riguardano. Vogliamo quindi cambiare la rotta dell’astensionismo culminata in particolare con le elezioni del 2023, dove un clarense su due ha scelto di non recarsi alle urne, campanello d’allarme che  deve indurci a riflettere rispetto a nuove forme di coinvolgimento nella vita democratica della Cittadinanza. La Politica deve quindi riavvicinarsi al Cittadino, ascoltarlo, porlo nelle condizioni di sentirsi partecipe alla costruzione di una comunità più coesa, cominciando innanzitutto dai luoghi che vive più quotidianamente e dove meglio può esprimere le proprie problematiche e proporre delle soluzioni. Desideriamo quindi coinvolgere la Cittadinanza nei processi decisionali e desideriamo consultarla periodicamente. 

## P﻿roposte

50. Creare nel corso del mandato almeno quattro Consigli di Quartiere -idealmente basati sulla quadripartizione delle Quadre – che possano essere luoghi di maggior contatto tra l’Amministrazione e la Popolazione: ciò per garantire una prossimità ed  uno scambio di informazioni maggiore e, naturalmente,la maggior possibilità di offrire risposte in tempi ridotti mediante canali di comunicazione più diretti fra Territorio e Amministrazione.
51. Creare, accanto ai Consigli di Quartiere, una ConsultaGiovani  ossia uno spazio di confronto di fiducia tra i Giovani e l’Amministrazione Comunale affinché si possano assicurare percorsi di inserimento lavorativo e di valorizzazione delle creatività oltre che collegamenti con le Associazioni di Volontariato/Culturali e Sportive della Città, consentendo ai giovani stessi di organizzare eventi ed attività per i coetanei e la Città.
52. Creare percorsi di coinvolgimento attivo quale il progetto “Rigenera il tuo Quartiere”,  con lo scopo di delineare vari tipi di miglioramenti: cura delle infrastrutture per la mobilità, riqualificazione di aree sotto utilizzate (come alcuni parchi) manutenzione su vialetti,  marciapiedi e arredo urbano, controllo e cura dei cestini per la raccolta rifiuti e degli spazi a parcheggio, abbattimento delle barriere architettoniche, rifacimento del verde (ad esempio Via Silvio Pellico, Via Tito Speri, Via Vivaldi, Via Forze Armate), e arricchimento degli spazi verdi con ulteriori piantumazioni (ove possibile) e ulteriori servizi quali aree per cani, aree gioco, servizi smart pubblici quali wi-fi, fino all’installazione di panchine.
53. Coinvolgere i Quartieri nelle azioni di più ampio respiro che li interessino, soprattutto negli ambiti di rigenerazione, mediante la realizzazione di progetti dedicati attraverso il “bilancio partecipativo” o mediante una loro consultazione nella revisione del PGT.
54. Consultazione in merito a progetti specifici che coinvolgano i quartieri per migliorarne e qualificarne la progettualità, ponendo attenzione all’organizzazione degli interventi.
55. Potenziare il ruolo del referendum cittadino, consentendo così che la Cittadinanza possa avere l’occasione di partecipare alle scelte decisionali del Comune mediante questi strumenti di democrazia diretta;
56. Rendere sempre più attivo il ruolo dei Consiglieri Comunali, approfittando della vicinanza che questi possono avere coi quartieri che anch’essi vivono, in un’ottica di rafforzamento del concetto di prossimità, mutuando per certi versi il modello americano per cui ogni eletto rappresenta davvero il territorio che l’ha scelto. Incentivare così un raccordo fra una promozione di partecipazione “dal basso” e un avvicinamento della democrazia rappresentativa.