---
title: Casa e Housing Sociale
subtitle: Rigenerare, Rinnovare, Riunire
excerpt: La nuova Amministrazione di Chiari mira a migliorare il benessere
  collettivo, affrontando la riduzione del consumo di suolo e la povertà
  abitativa ed energetica. Priorità è data alla rigenerazione urbana, con la
  creazione di un fondo di social housing per trasformare aree come l'“Area ex
  NK” e rigenerare edifici sottoutilizzati. Si promuove la locazione a canone
  concordato, soprattutto nei quartieri storici, e si prevede di aggiungere 150
  unità abitative per i più vulnerabili entro cinque anni. L'approccio è
  policentrico e inclusivo, con servizi come il portierato sociale e alloggi di
  foresteria per rispondere a esigenze temporanee, garantendo accesso a
  soluzioni abitative sostenibili.
order: 3
---
## P﻿remessa

Gli spazi fisici ed urbanistici di una Città nel corso del tempo sono oggetto di trasformazione e rinnovamento, ma tali cambiamenti devono essere governati dalla Politica, la quale deve agire con progettualità attraverso gli strumenti di cui dispone e deve mirare al benessere della collettività a cui è a servizio. In tale ottica è evidente che la nuova Amministrazione dovrà fare i conti con diverse e consistenti sfide, in primis quelle dell’abbattimento del consumo di suolo libero – secondo le direttrici comunitarie e regionali – nonchè del contrasto alla povertà abitativa (intesa anche come scarsità o poca efficienza della connessione e nell’accesso ai servizi pubblici) ed energetica. Vogliamo quindi venire incontro ad un tema molto sentito nella nostra comunità, considerando che nelle aree residenziali vive il 60% della popolazione clarense.

## P﻿roposte

11. L’Amministrazione dovrà essere protagonista nel promuovere politiche di edilizia sociale pubblica e privata (per anziani, lavoratori fuori sede, giovani coppie, famiglie monogenitoriali) attraverso la costituzione di un fondo immobiliare di social housing con la Cassa depositi e Prestiti intervenendo, in particolare, sugli ambiti di rigenerazione già presenti nel Documento di Piano del PGT (si pensi alla c.d. “Area ex NK”, con il miglioramento della viabilità di quartiere) e altri da individuarsi principalmente sui quartieri più vecchi della Città e/o con iniziative autonome della Amministrazione.
12. Promuovere una gestione condivisa e strategica del PGT, partendo da una mappatura effettiva tesa alla ricognizione del patrimonio immobiliare pubblico di cui dispone o di cui potrebbe disporre. Da tale mappatura si potrebbe anche dedurre il patrimonio edilizio ammalorato e sottoutilizzato per rigenerarlo e quindi agire per la ristrutturazione di abitazioni sfitte, con particolare attenzione al Centro Storico ed ai quartieri più vecchi, da destinare alla locazione convenzionata a canone concordato utilizzando con incisività gli strumenti della rigenerazione urbana, incentivi comunali già previsti ed eventualmente intervenendo sugli oneri di ristrutturazione (attraverso la realizzazione di interventi di riqualificazione energetica/sismica). I contratti di locazione a canone concordato (più bassi rispetto al mercato quindi) potrebbero essere favoriti mediante una compensazione verso i proprietari in ragione delle minori entrate.
13. Offrire alla Cittadinanza la creazione, nei 5 anni di mandato, di circa 150 nuove unità abitative residenziali per giovani/anziani/lavoratori e per le fasce deboli della popolazione
14. Applicare un criterio solidaristico e perequativo nell’assegnazione e nelle modalità di gestione dell’edilizia pubblica abitativa che tenga conto delle situazioni di maggior disagio e difficoltà (si pensi alle numerose famiglie di stranieri o di italiani di seconda generazione, a persone anziane con poca o assente assistenza famigliare etc);
15. Adottare un approccio policentrico nella valorizzazione delle aree da riqualificare dando parola, spazio ed opportunità alle persone che in quei quartieri ci abitano, facendo di Chiari una città policentrica, ma integrata
16. Collaborare con ALER per creare un servizio di portierato sociale con particolare attenzione alle fragilità ed alle situazioni di svantaggio presenti;
17. Promuovere servizi di foresteria affinché si possa fornire un servizio di alloggio breve in attesa di trovare soluzioni abitative di più lunga durata confacenti alle diverse disponibilità di ciascuno, sia delle persone che lavorano o che potrebbero lavorare a Chiari, sia a coloro che, per motivi di studio o di lavoro hanno bisogno di un riferimento abitativo a Chiari e da qui si collegano facilmente nelle città come Brescia o Milano.