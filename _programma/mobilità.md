---
title: Mobilità
subtitle: Percorsi Accessibili, Città Connessa
excerpt: Chiari possiede circa 100 km di strade, inclusi 20% di piste ciclabili,
  che richiedono manutenzione regolare per un sistema di mobilità sostenibile.
  La nuova Amministrazione mira a riformare la mobilità dolce, valorizzando le
  biciclette e migliorando il servizio di bus navetta, con un'attenzione
  speciale per anziani e disabili. Si prevede di espandere la rete ciclabile a
  40 km, garantire la manutenzione delle piste esistenti, e aumentare
  l'accessibilità alla stazione ferroviaria. Altre misure includono potenziare
  la segnaletica per ciclisti, migliorare l'interconnettività con comuni
  limitrofi e attuare un piano per l'abbattimento delle barriere
  architettoniche, aumentando la fruibilità della città per tutti, inclusi non
  vedenti e ipovedenti, con soluzioni tattili e cromatiche lungo percorsi
  strategici.
order: 5
---
## P﻿remessa

La Città di Chiari ha circa 100 km di strade tra urbane e extraurbane, di cui oltre il 20% è costituito da piste ciclabili. Una rete capillare su tutto il territorio che necessita di programmi manutentivi ordinari capaci di poter creare un sistema lineare, periodico ed efficiente degli interventi stessi. Ma non solo manutenzione: il sistema viabilistico clarense richiede anche un cambio di rotta volto a ripensare il ruolo della mobilità dolce quale strumento semplice, alla mano, ma efficiente e consono ad una realtà a misura del cittadino-ciclista. La biciletta è l’esempio emblematico della mobilità dolce che la nuova Amministrazione si propone di valorizzare, ma non è l’unico. Anche il potenziamento del sistema di bus navetta è necessario per garantire la piena mobilità della Cittadinanza, con un’attenzione particolare ai soggetti a ridotta mobilità come anziani o disabili. A favore proprio di questi ultimi – ed in generale verso le persone con difficoltà motorie o di altro genere - la nuova Amministrazione intende sia dare piena attuazione al Piano per l’Abbattimento delle Barriere Architettoniche, sia promuovere soluzioni che si indirizzino a potenziare le possibilità di movimento di queste categorie, consapevole che le Barriere non sono solo quelle fisiche e di più facile individuazione, ma anche tutti quegli ostacoli di varia natura – e quindi anche sensoriali - che si frappongono ad un pieno godimento e fruibilità della Città e dei suoi luoghi. 

## P﻿roposte

34. Potenziare e capillarizzare, in un’ottica di decarbonizzazione della Città, il sistema della viabilità ciclabile, con l’obbiettivo di raggiungere i 40 km complessivi, ossia quasi la metà della rete viabilistica cittadina
35. Garantire una manutenzione accorta ed attenta delle piste ciclabili al fine di favorirne il suo utilizzo, dando esecuzione ad accordi con privati, rimasti inattuati, per il riordino e il ripristino del manto stradale
36. Rendere maggiormente accessibile e fruibile la stazione ferroviaria, nel rispetto delle rispettive competenze e in dialogo e coordinamento con gli Enti coinvolti. È noto e ben visibile l’assenza di un ascensore-montacarichi che rende estremamente difficoltosa la salita e la discesa nel sottopassaggio per il raggiungimento del binario due.
37. Potenziare e capillarizzare il servizio di bus navetta presente a Chiari aumentandone le fermate presso le postazioni di parcheggio.
38. Rendere le piste ciclabili centro della mobilità cittadini ed extracittadina, prevedendo indicazioni di percorso, indicazioni dei luoghi pubblici più rilevanti per la Città (es: Comune, Biblioteca, Scuola, Ospedale etc), oltre che per fa conoscere luoghi di interesse storico, artistico e sociale.
39. Sviluppare l’interconnettività con i comuni limitrofi, ampliando e diramando maggiormente i collegamenti con i Comuni vicini collegati fino ad oggi da strade non alla portata dei ciclisti (es: strada per Castelcovati);
40. Attuare il Piano per l’Abbattimento delle Barriere Architettoniche mediante una mappatura delle barriere presenti e quindi la realizzazione di rampe, ascensori, scivoli nei pressi di scale e altri punti di salita o uscita per ovviare agli ostacoli alla mobilità delle persone con difficoltà motorie;
41. Abbattere le altre tipologie di barriere che non consentono, soprattutto per le persone con disabilità o difficoltà di varia natura, la piena fruibilità della Città. La nuova Amministrazione vorrebbe puntare sul potenziamento e ampliamento dei semafori sonori (anche nelle piazze, in stazione, sulle vie di maggiore percorrenza) così come sulla creazione di “loges” cioè di un sistema organico di soluzioni tattili (segni e superfici in rilievo) e/o cromatiche (segnaletiche con utilizzo di colori maggiormente contrastanti) di facile intuizione ed installati in punti strategici o di maggior pericolo che aiuterebbero le persone non vedenti o ipovedenti a  muoversi in autonomia e sicurezza lungo percorsi prestabiliti;
42. Potenziare i parcheggi per disabili e quelli dedicati ai genitori con figli di età inferiore ai 3 anni nonché, dato l’invecchiamento progressivo della popolazione, promuovere la creazione dedicata alle persone anziane.