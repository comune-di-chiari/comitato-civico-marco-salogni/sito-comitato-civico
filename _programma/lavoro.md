---
title: Lavoro
subtitle: Il Comune al Tuo fianco
excerpt: A Chiari, il lavoro è visto come strumento di emancipazione e sviluppo.
  L'amministrazione promuove l'adozione di certificazioni come "ESG" per
  incrementare la visibilità e la responsabilità sociale delle imprese. Si mira
  a facilitare l'inserimento lavorativo dei giovani e dei lavoratori stranieri
  attraverso la formazione e il supporto continuo. Un "sportello amico" aiuterà
  nella riduzione della burocrazia. Si prevede l'assunzione diretta di personale
  per i servizi comunali e programmi specifici per l'inserimento di persone con
  disabilità, assicurando integrazione e rispetto della dignità.
order: 11
---
## P﻿remessa

Fonte essenziale di sostentamento, mezzo indispensabile di emancipazione, strumento per la realizzazione delle proprie ambizioni e desideri, il lavoro è un elemento fondamentale in ogni linea programmatica politica, soprattutto perché è necessità essenziale influenzata da fattori molteplici, soggetta a cambiamenti sociali ed economici, componente di visioni politiche contrapposte. Calando il discorso nella realtà clarense vediamo che le attività economiche a Chiari sono molteplici e variegate, così come eterogenee e, pertanto, multiple sono le risposte da dare e le problematiche da affrontare insieme.

## P﻿roposte

79. Attivare un costante dialogo con le diverse realtà produttive presenti sul territorio, così da intercettarne i bisogni e le esigenze e porsi quale promotore e facilitatore del loro sviluppo. Ad esempio, sempre più società intendono dotarsi della certificazione “ESG – Environmental and Social Governance” che consente alle aziende di avere maggiore visibilità, anche verso eventuali investitori, oltre che fornire un sistema produttivo più attento alle istanze sociali ed ambientali.
80. Favorire meccanismi di inserimento dei giovani nel mondo del lavoro, creando una rete di imprese e incentivare l’azione propositiva degli strumenti di inserimento quali la Consultagiovani e Informagiovani.
81. Prevedere la creazione di uno sportello amico per facilitare procedure di inserimento e di re-inserimento nel mondo del lavoro e le pratiche legate alla burocrazia riferita a tutti i cittadini.
82. Incentivare un dialogo costante con i lavoratori, con particolare attenzione a quelli di origine straniera, aiutando nella formazione linguistica e pertanto nel corretto e sicuro svolgimento delle dinamiche produttive.
83. Favorire l’assunzione diretta presso il Comune o attraverso la Municipalizzata Chiari Servizi dei dipendenti dei servizi erogati in modo continuo dal Comune (Assistenti Sociali, Asilo Nido, Pulizia/Igiene edifici,etc) e che oggi sono appaltati a Cooperative esterne.
84. Prevedere in particolare un programma di inserimento nel mondo del lavoro per ragazzi con disabilità una volta terminato il ciclo scolastico con attenzione valorizzando le loro capacità in accordo con la propria disabilità e senza ledere la loro dignità.