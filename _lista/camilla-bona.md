---
name: Camilla
surname: Bona
summary: 30 anni, laureata in scienze internazionali ed istituzioni europee,
  nata e cresciuta a Chiari, aiuto i cittadini clarensi a realizzare il loro
  sogno di casa  in qualità di agente immobiliare. Candidandomi, ho deciso di
  rendere concreto l’amore per Chiari offrendo impegno e determinazione.
order: 2
image: /assets/uploads/camilla.jpg
image_alt: Foto di Camilla a mezzo busto sorridente in giacca smanicata e
  camicia. Alle spalle via Villatico che imbocca piazza Zanardelli
is_candidate: false
cv_url: /assets/uploads/cv-camilla-bona.pdf
casellario_giudiziario_url: /assets/uploads/bona-camilla.pdf
---
