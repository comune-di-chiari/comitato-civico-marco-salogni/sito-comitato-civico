---
name: Francesca
surname: Galli
summary: 44 anni, mi occupo di programmazione dell’attività produttiva
  nell’azienda di famiglia, laureata in ingegneria gestionale, Svolgo da 10 anni
  volontariato per il Rusticò Belfiore e da 1 anno in Protezione Civile. Ho
  deciso di mettermi al servizio a fianco di Marco perchè credo nel progetto e
  ne condivido i valori.
order: 10
image: /assets/uploads/francesca-galli.jpg
image_alt: Francesca Galli sorridente a mezzo busto con un dolcevita verde
  scuro. Dietro una siepe sfocata
is_candidate: false
cv_url: /assets/uploads/francesca-galli.pdf
casellario_giudiziario_url: /assets/uploads/galli-francesca.pdf
---
