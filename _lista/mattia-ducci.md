---
name: Mattia
surname: Ducci
summary: 31 anni, programmatore e consulente informatico nel campo
  dell'accessibilità. Mi candido per contribuire a rendere Chiari una città più
  accessibile, inclusiva e in grado di abbattere barriere.
order: 12
image: /assets/uploads/mattia.jpg
image_alt: Foto ritratto di Mattia Ducci con sfondo sfocato di un muro e una siepe
is_candidate: false
cv_url: /assets/uploads/cv4327936.pdf
casellario_giudiziario_url: /assets/uploads/ducci-mattia.pdf
---
