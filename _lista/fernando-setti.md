---
name: Fernando
surname: Setti
summary: "67 Anni, sposato con figlie, diplomato in elettronica, pensionato,
  volontario presso il Faro 50.0 e SPI CGIL di Chiari. Mi candido con Marco
  Salogni perché “ci si salva e si agisce insieme e non solo uno per uno” ( E.
  Berlinguer). "
order: 15
image: /assets/uploads/fernando-setti.jpg
image_alt: Primo piano di Fernando Setti con Giubbino di pelle marrone e sciarpa
  leggera arancio che sorride alla camera. Sullo sfondo, sfocate, parte di una
  scultura e di una siepe
is_candidate: false
cv_url: /assets/uploads/fernando-setti-cv.pdf
casellario_giudiziario_url: /assets/uploads/setti-fernando.pdf
---
