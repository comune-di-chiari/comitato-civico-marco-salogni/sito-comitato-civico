---
name: Ernesto
surname: Cancelli
summary: 58 anni, imprenditore agricolo, nato e cresciuto a Chiari. Ho sempre
  operato nell’ambito agricolo clarense, dove ho ricoperto ruolo di Presidente
  di Coldiretti e del Consorzio irriguo Baiona. Condivido il progetto e
  l’attenzione ai temi della sostenibilità e all’agricoltura.
order: 13
image: /assets/uploads/ernesto-cancelli.jpg
image_alt: Ernesto Cancelli con giubbino scamosciato e camicia che sorride. Alle
  spalle una scultura appesa ad un muro bianco
is_candidate: false
cv_url: /assets/uploads/cv-ernesto-cancelli.pdf
casellario_giudiziario_url: /assets/uploads/cancelli-ernesto.pdf
---
