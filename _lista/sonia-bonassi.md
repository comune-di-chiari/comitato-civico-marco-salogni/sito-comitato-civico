---
name: Sonia
surname: Bonassi
summary: 30 anni, nata e cresciuta a Chiari. Lavoro come psicologa specializzata
  in Psicoterapia dell'Adolescente e del Giovane Adulto. Ritengo che i giovani
  rappresentino una risorsa fondamentale; porto il mio contributo per
  valorizzare i loro bisogni e per consolidare l’alleanza scuola-famiglia.
order: 4
image: /assets/uploads/sonia-bonassi.jpg
image_alt: Sonia Bonassi a braccia conserte elegante con giacca nera. Sullo
  sfondo un muro bianco
is_candidate: false
cv_url: /assets/uploads/cvsoniabonassi2024.pdf
casellario_giudiziario_url: /assets/uploads/bonassi-sonia.pdf
---
