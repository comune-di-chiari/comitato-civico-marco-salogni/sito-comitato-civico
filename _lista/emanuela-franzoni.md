---
name: Emanuela
surname: Franzoni
summary: Ho insegnato alla Scuola Primaria per 38 anni, per la maggior parte a
  Chiari, presso la scuola Turla. Credo fortemente nella scuola e
  nell'istruzione, per dare alle nuove generazioni un'offerta formativa sempre
  al passo con i tempi. Credo in Marco. La scelta migliore!
order: 14
image: /assets/uploads/emanuela-franzoni.jpg
image_alt: "Emanuela Franzoni con maglione blu e camicia che sorride in primo
  piano. Ha gli occhiali da sole e la foto è molto soleggiata. Alle spalle una
  siepe "
is_candidate: false
cv_url: /assets/uploads/cv-franzoni.pdf
casellario_giudiziario_url: /assets/uploads/franzoni-emanuela.pdf
---
