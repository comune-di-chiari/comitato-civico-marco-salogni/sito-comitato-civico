---
name: Marco
surname: Salogni
summary: 35 anni, nato a cresciuto a Chiari. Laureato in Ingegneria Civile
  all'Università di Brescia. Già presidente della municipalizzata di Chiari
  Servizi Srl, Liquidatore Unico della Società COGES e attualmente Responsabile
  flotte. Mi candido alla carica di Sindaco per Chiari, per la mia Città,
  ripartendo dai Suoi cittadini, per costruire una Città più inclusiva ed
  attenta ai bisogni delle Persone
order: 0
image: /assets/uploads/marco-salogni.jpg
image_alt: Foto di Marco Salogni in primo piano in giacca e cravatta,
  sorridente. Sfondo bianco
is_candidate: true
cv_url: /assets/uploads/salogni-marco-1-.pdf
casellario_giudiziario_url: /assets/uploads/marco-salogni.pdf
---
