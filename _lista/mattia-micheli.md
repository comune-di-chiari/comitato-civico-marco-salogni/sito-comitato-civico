---
name: Mattia
surname: Micheli
summary: 30 anni, infermiere. Ho lavorato in una clinica irlandese, poi nel
  reparto Covid per due anni e in collaborazione ho gestito l’hub vaccinale
  presso il campo sportivo clarense; oggi sono al servizio di immunoematologia e
  medicina trasfusione dell'ospedale di Chiari. Credo che sia fondamentale
  mettere le persone e i loro bisogni al centro.
order: 8
image: /assets/uploads/mattia-micheli.jpg
image_alt: Mattia Micheli in camicia bianca e braccia conserte con uno sguardo
  deciso e un sorriso abbozzato. Alle spalle, sfocati, degli alberi e un viale
is_candidate: false
cv_url: /assets/uploads/cv-mattia-micheli.pdf
casellario_giudiziario_url: /assets/uploads/micheli-mattia.pdf
---
