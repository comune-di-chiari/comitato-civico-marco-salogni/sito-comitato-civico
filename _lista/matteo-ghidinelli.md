---
name: Matteo
surname: Ghidinelli
summary: 30 anni, project manager nell'ambito della finanza agevolata e della
  consulenza aziendale, dottore in scienze politiche e in cultura della
  comunicazione, nato e cresciuto a Chiari,  Ho deciso di candidarmi perché
  credo nella costruzione di una città a misura di ogni cittadino, una Chiari
  che non lascia indietro nessuno.
order: 7
image: /assets/uploads/matteo-ghidinelli.jpg
image_alt: Matteo Ghidinelli in primo piano in camicia col colletto aperto e
  giacca. Alle spalle, sfocata, una siepe
is_candidate: false
cv_url: /assets/uploads/cv_ghidinelli.pdf
casellario_giudiziario_url: /assets/uploads/matteo-ghidinelli.pdf
---
