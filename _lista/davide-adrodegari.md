---
name: Davide
surname: Adrodegari
summary: 34 anni, giurista d’impresa presso una società
  multinazionale,  laureato  in giurisprudenza. Candidandomi,  vorrei
  valorizzare, insieme alle altre proposte, il legame inscindibile fra Ambiente
  e Uomo per una Chiari coesa e solidale, inclusiva ed aperta, intraprendente e
  coraggiosa, verde e sostenibile.
order: 1
is_candidate: false
image: /assets/uploads/davide_adro.jpg
image_alt: Foto mezzo busto di Davide Adrodegari in giacca, camicia e cravatta e
  braccia conserte che guarda sorridente l'obiettivo. Alle spalle un parco
cv_url: /assets/uploads/20240513-cv-europass-davide-adrodegari.pdf
casellario_giudiziario_url: /assets/uploads/adrodegari-davide.pdf
---