---
name: Giacomo
surname: Loschi Della Torre
summary: 34 anni, avvocato, nato e cresciuto a Chiari. maturità scientifica
  presso l'Istituto Salesiano San Bernardino. Fondatore e primo Presidente
  dell’Associazione Sala Studio clarense Falcone e Borsellino. Credo nel
  progetto di Marco Salogni Sindaco, perché mi riconosco nei valori e negli
  ideali che costituiscono l’essenza della sua candidatura.
order: 9
image: /assets/uploads/giacomo-loschi.jpg
image_alt: Giacomo Loschi in primo piano in camicia che guarda sorridente l'obiettivo
is_candidate: false
cv_url: /assets/uploads/giacomo-cv.pdf
casellario_giudiziario_url: /assets/uploads/loschi-della-torre-giacomo.pdf
---
