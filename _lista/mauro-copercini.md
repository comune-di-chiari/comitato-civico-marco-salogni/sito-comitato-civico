---
name: Mauro
surname: Copercini
summary: "49 anni, avvocato, abito e lavoro a Chiari. Sono stato soccorritore
  per la Croce Rossa Italiana, ho deciso di candidarmi al servizio della mia
  Città in quanto credo nella partecipazoine attiva quale forma di crescita
  della nostra Comunità. "
order: 5
image: /assets/uploads/mauro-copercini.jpg
image_alt: Mauro Copercini in giacca e cravatta in una fotografia in primo piano
  mentre guarda leggermente fuori dall'obiettivo della fotocamera
is_candidate: false
cv_url: /assets/uploads/mauro-copercini_cv-2-.pdf
casellario_giudiziario_url: /assets/uploads/mauro-copercini.pdf
---
