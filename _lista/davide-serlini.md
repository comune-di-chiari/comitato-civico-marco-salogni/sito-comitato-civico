---
title: Davide Serlini
name: Davide
surname: Serlini
summary: 54 anni, artigiano edile, maturità tecnica, impegnato nel volontariato
  oratoriano. Ho deciso di candidarmi perché credo fortemente in Marco Salogni e
  nel suo progetto per una Chiari inclusiva, solidale, sostenibile, dinamica e
  attenta  ai bisogni delle persone.
order: 3
image: /assets/uploads/davide-serlini.jpg
image_alt: Foto in primo piano di Davide Serlini in camicia e maglione,
  sorridente con dietro lo sfondo sfocato di una siepe
is_candidate: false
cv_url: /assets/uploads/serlini_davide_cv.pdf
casellario_giudiziario_url: /assets/uploads/serlini-davide.pdf
---
