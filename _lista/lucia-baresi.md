---
name: Lucia
surname: Baresi
summary: "medico ostetrico-ginecologo con studio medico a Chiari. Ho svolto
  attività di Medico ginecologo libero professionista in diverse realtà
  sanitarie. Ho ricoperto la carica di Assessore ai servizi sociali per il
  Comune di Chiari. "
order: 11
image: /assets/uploads/lucia-baresi.jpg
image_alt: Lucia Baresi che ride in modo spontaneo. Foto a mezzo busto
is_candidate: false
cv_url: /assets/uploads/cv-baresi-lucia.pdf
casellario_giudiziario_url: /assets/uploads/baresi-lucia.pdf
---
