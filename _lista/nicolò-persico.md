---
name: Nicolò
surname: Persico
summary: 34 anni, vivo da sempre a Chiari. Laureato in Economia, responsabile
  amministrativo in un’azienda di consulenza energetica, corridore dell’Atletica
  Chiari e per quadra Zeveto, Presidente dell’associazione Biathlon Azzurro.
  Credo nella condivisione e nel fare rete tra le realtà del territorio, metto a
  disposizione della comunità le mie competenze e la mia passione.
order: 16
image: /assets/uploads/nicolo-persico.jpg
image_alt: Nicolò Persico in camicia che sorride in primo piano di fronte alla
  fotocamera. Alle spalle una scultura appesa ad un muro
is_candidate: false
cv_url: /assets/uploads/cv-nicolò-persico-1-.pdf
casellario_giudiziario_url: /assets/uploads/persico-nicolo.pdf
---
