---
name: Giovanna
surname: Libretti
summary: 61 anni, infermiera con esperienza verso le persone con
  bisogni  assistenziali  complessi  correlati  a malattie  cronico-
  degenerative. Mi dedico al volontariato. L’attenzione ai bisogni delle persone
  e la ricerca di soluzioni condivise, con competenza, sono state le direttrici
  del mio modo di operare, obiettivi che intendo continuare a perseguire
order: 6
image: /assets/uploads/giovanna-libretti.jpg
image_alt: Foto mezzo busto di Giovanna Libretti con alle spalle una scultura
  appesa ad un muro. Giovanna si presenta sorridente in giacca, maglia bianca e
  collana
is_candidate: false
cv_url: /assets/uploads/cv-giovanna-libretti-2024.pdf
casellario_giudiziario_url: /assets/uploads/libretti-giovanna.pdf
---
