---
layout: post
title: Gli incontri di quartiere della settimana 20-26 maggio
date: 2024-05-20T08:59:26.668Z
featured_image: /assets/uploads/whatsapp-image-2024-05-19-at-12.55.28.jpeg
featured_image_alt: Locandina con scritti gli incontri di quartiere riportati nel testo
---
Siamo felici di invitare tutti i cittadini agli incontri di quartiere che si terranno nei prossimi giorni. Questi incontri rappresentano un’opportunità unica per tutti i membri della comunità di partecipare attivamente alla vita politica locale, esprimere le proprie idee e contribuire con proposte concrete. Gli incontri si terranno nelle seguenti date:

* L﻿unedì 20 maggio alle ore 20:45 al parcheggio del Centro Sportivo
* G﻿iovedì 23 maggio alle ore 20:30 al Parco Elettra
* S﻿abato 25 maggio alle ore 19 in Piazza delle Erbe