---
layout: post
title: Gli incontri di quartiere della settimana dal 27 maggio al 2 giugno
date: 2024-05-27T05:23:00.123Z
featured_image: /assets/uploads/locandina-crop.png
featured_image_alt: Locandina con scritti gli incontri di quartiere riportati nel testo
---
Siamo felici di invitare tutti i cittadini agli incontri di quartiere che si terranno nei prossimi giorni. Questi incontri rappresentano un’opportunità unica per tutti i membri della comunità di partecipare attivamente alla vita politica locale, esprimere le proprie idee e contribuire con proposte concrete. Gli incontri si terranno nelle seguenti date:

* L﻿unedì 27 maggio alle ore 20:30 alla zona ex Brasilen
* Martedì 28 maggio alle ore 20:45 al parcheggio del Campo Sportivo
* Mercoledì 29 maggio alle ore 20:30 in via Monte Bianco numero 15
* Giovedì 30 maggio alle ore 20:30 al parcheggio della scuola San Giovanni
* Venerdì 31 maggio alle ore 20:45 al Parco degli Alpini