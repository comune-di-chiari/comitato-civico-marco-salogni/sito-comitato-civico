---
layout: post
title: Vi aspettiamo all' aperitivo di fine Campagna Elettorale
date: 2024-06-06T05:18:07.327Z
featured_image: /assets/uploads/whatsapp-image-2024-06-06-at-07.17.42.jpeg
featured_image_alt: "Locandina con la data dell'aperitivo di fine campagna
  elettorale: Giovedì 6 giugno ore 20:30 piazza Zanardelli a Chiari"
---
La nostra avventura elettorale è giunta al termine e non c'è modo migliore per celebrarla se non con una bella festa tutti insieme!

📅 **Quando**: 06/06/2024

⏰ **Ora**: 20:30

📍 **Dove**: Piazza Zanardelli, Chiari

Sarà un'occasione speciale per ringraziarvi tutti per il vostro incredibile supporto. Vi aspettiamo per un po' di chiacchiere, musica, cibo e risate

Non mancate! Vi aspettiamo!