module Jekyll
  class ImageTagClassAdder < Converter
    safe true
    priority :low

    def matches(ext)
      ext =~ /^\.md$/i
    end

    def output_ext(ext)
      ".html"
    end

    def convert(content)
      content.gsub(/<img src="([^"]+)" alt="([^"]*)" \/>/) do
        "<img src=\"#{$1}\" alt=\"#{$2}\" class=\"img-fluid\" />"
      end
    end
  end
end
