module Jekyll
  module FilenameWithoutExtension
    def filename_no_ext(input)
      File.basename(input, ".*")
    end
  end
end

Liquid::Template.register_filter(Jekyll::FilenameWithoutExtension)
